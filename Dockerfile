FROM node:14

WORKDIR /usr/src/app

COPY . .

RUN npm install 
RUN npm install -g typescript

RUN ls -la

RUN npx tsc

EXPOSE $PORT

CMD [ "npm", "run", "start" ]